package com.example.janke.dtf;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.jtransforms.fft.DoubleFFT_1D;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Wykres extends FFT {
    LineChart chart;
    private MyTask myTask=new MyTask();
    private TextView tekstView;
    private ArrayList<Entry> values = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstance) {

        super.onCreate(savedInstance);

        Intent mIntent = getIntent();
        int intValue = mIntent.getIntExtra("int_variable", 0);
        setContentView(R.layout.activity_wykres);
        tekstView= findViewById(R.id.editText);
        chart = (LineChart) findViewById(R.id.Linechart);
        chart.setHighlightPerDragEnabled(true);
        chart.setBackgroundColor(Color.GRAY);
        chart.setMaxVisibleValueCount(50);
        chart.setPinchZoom(false);
        chart.setDrawGridBackground(true);
        chart.setMaxVisibleValueCount(5);
        LineData data = new LineData();
        data.setValueTextColor(Color.BLACK);
        chart.setData(data);
        XAxis x1 = chart.getXAxis();
        x1.setTextColor(Color.BLACK);
        x1.setDrawGridLines(false);
        x1.setAvoidFirstLastClipping(true);
        x1.setAxisMaximum(1000f);
        YAxis y1 = chart.getAxisLeft();
        y1.setTextColor(Color.BLACK);
        y1.setAxisMaximum(440f);
        y1.setDrawGridLines(true);
        y1.setLabelCount(6);

        YAxis y2 = chart.getAxisRight();
        y2.setEnabled(false);
        ArrayList<Entry> values = new ArrayList<>();
        addEntry(10,values.size()+1,values);

        myTask.execute(intValue);

    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        myTask.cancel(true);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        myTask.cancel(true);
    }
    @Override
    protected void onDestroy() {

        super.onDestroy();

        myTask.cancel(true);
    }

    public void addEntry(double a, int b, ArrayList<Entry> values ){
        LineDataSet set1;
        float g = (float) a;
        values.add(new Entry(b, g));

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chart.moveViewToX(values.size()-5);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
            chart.setVisibleXRangeMaximum(5f);
            chart.animateX(200);
            chart.invalidate();
        } else {
            set1 = new LineDataSet(values, "Sample Data");
            set1.setDrawIcons(false);
            set1.enableDashedLine(10f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.DKGRAY);
            set1.setCircleColor(Color.DKGRAY);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(false);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            LineData data = new LineData(dataSets);
            chart.setData(data);
        }

    }

class bigEnd1{
    public double freq;
    public double mag;
    public bigEnd1(int a, double b){
        freq=a;
        mag=b;
    }
    public double getFreq(){
        return freq;

    }}

class MyTask extends AsyncTask<Integer,Void,Void> {
    /*Wartosci potrzebne do konfiguracji*/
    private static final int Fs_RATE=8000; //Hz
    private static final int CHANELS= AudioFormat.CHANNEL_IN_MONO;
    private static final int AUDI0_ENCODING=AudioFormat.ENCODING_PCM_16BIT;
    private static final int BUFFER_SIZE=2048;
    /*Wartosci okreslajace wielkosc transformaty*/
    private static final int SAMPLE_SIZE = 2048;//2048
    private static final int SAMPLE_AND_ZERO_PADDING = 2*SAMPLE_SIZE;//2*
    private static final int FFT_SIZE = SAMPLE_AND_ZERO_PADDING*2;
    private static final int DECIM = 2;
    /*Inne zmiene wykorzystane w metodach klasy*/
    private AudioRecord recordAudio=new AudioRecord(MediaRecorder.AudioSource.MIC, Fs_RATE, CHANELS, AUDI0_ENCODING, BUFFER_SIZE);
    private short [] inputData;
    private double [] inputDataDecim=new double[SAMPLE_SIZE];
    private double [] postWindow;
    private boolean windowIsSet= false;
    private double [] window=new double[SAMPLE_SIZE];
    private double d=0;
    private String mass="";
    private double All[][]= new double[12][4];
    private bigEnd1 pocht[]= new bigEnd1[1000];



    @Override
    protected Void doInBackground(Integer... voids) {
        Thread holdon = new Thread();
        final ArrayList<BarEntry> bar = new ArrayList<>();
        inputData = new short[SAMPLE_SIZE];
        getTuneHz(voids);
        try{
            while(true){
                if(recordAudio.RECORDSTATE_RECORDING!=recordAudio.getRecordingState()){
                    recordAudio.startRecording();
                }
                recordAudio.read(inputData,0,SAMPLE_SIZE);
                if(windowIsSet==false){
                    setWindow();

                }
                decymacja();
                averege();
                Hammering();
                d=FFTprocesing();
                resultOfTunning();
                Log.d("CREATION",""+d);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tekstView.setText(""+mass);
                        if(d!=0)
                        addEntry(d,values.size()+1,values);
                    }
                });
                holdon.sleep(1000);

                //saveData();

            }}
        catch (InterruptedException e){recordAudio.stop();
            recordAudio.release();}
        return null;
    }
    private void saveData(){
        String outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/dane.txt";
        try
        {
            FileOutputStream fos = new FileOutputStream(outputFile);
            DataOutputStream dos = new DataOutputStream(fos);
            String s="dddddddddddddddddddd";
            for(int i=0;i<2048;i++){
                s=Short.toString(inputData[i]);
                s=s+" ";
                dos.writeBytes(s);
            }//.writeShort(i);
            dos.close();
        }
        catch (IOException e)
        {
            System.out.println("IOException : " + e);
        }
    }
    private void getTuneHz(Integer[] wej){
        int hzr=wej[0];
        for(int i=0;i<4;i++){
            for(int j=0;j<12;j++){
                if(j==0)
                    All[j][i]=hzr;
                else
                    All[j][i]=All[j-1][i]*0.9442;
            }
            hzr=hzr/2;
        }
    }
    private void resultOfTunning(){
        if(d>All[0][0]&&d<All[11][3])
            mass=" "+0;
        else{
            for(int i=0;i<4;i++){
                for(int j=0;j<10;j++){
                    if(All[j][i]>=d && All[j+1][i]<=d)
                        displayResult(i,j);
                }
            }
        }
    }
    private void displayResult(int i, int j){
        double div=(All[j][i]-All[j+1][i])/2;
        Double pri;
        if(i==3){
            if((All[j][i]*0.99<d&&d<All[j][i]*1.01))
                mass="Perfect "+getLetter(j);
            else if(All[j+1][i]*0.99<d&&d<All[j+1][i]*1.01)
                mass="Perfect "+getLetter(j+1);
            else if((d-All[j+1][i])<div){
                pri=d-All[j+1][i];
                mass="Close to "+getLetter(j+1)+" "+pri.toString();
            }
            else{
                pri=d-All[j][i];
                mass="Close to "+getLetter(j)+" "+pri.toString();
            }
        }
        else{
            if((All[j][i]*0.999<d&&d<All[j][i]*1.0075))
                mass="Perfect "+getLetter(j);
            else if(All[j+1][i]*0.999<d&&d<All[j+1][i]*1.0075)
                mass="Perfect "+getLetter(j+1);
            else if((d-All[j+1][i])<div){
                pri=d-All[j+1][i];
                mass="Close to "+getLetter(j+1)+" "+pri.toString();
            }
            else{
                pri=d-All[j][i];
                mass="Close to "+getLetter(j)+" "+pri.toString();
            }
        }
    }
    private String getLetter(int j){
        if(j==0)
            return "A";
        else if(j==1)
            return "Gis";
        else if(j==2)
            return "G";
        else if(j==3)
            return "Fis";
        else if(j==4)
            return "F";
        else if(j==5)
            return "E";
        else if(j==6)
            return "Dis";
        else if(j==7)
            return "D";
        else if(j==8)
            return "Cis";
        else if(j==9)
            return "C";
        else if(j==10)
            return "B";
        else if(j==11)
            return "Ais";
        return "Nic";
    }
    private void decymacja(){
        for(int i=0;i<(SAMPLE_SIZE/DECIM)-1;i++)
            inputDataDecim[i]=inputData[DECIM*i];
    }

    private void averege() {
        double res=0;
        for (int i = 4; i < inputDataDecim.length-2; i++) {
            res=((inputDataDecim[i-4]+inputDataDecim[i-3]+inputDataDecim[i-2]+inputDataDecim[i-1]+inputDataDecim[i])/5);
            inputDataDecim[i]=res;
        }
    }

    private void setWindow(){
        windowIsSet=true;
        for(int i=0; i<SAMPLE_SIZE;i++)
            window[i] = 0.54-0.46*Math.cos(2*Math.PI*i/SAMPLE_SIZE);
    }
    private void Hammering(){
        postWindow = new double[SAMPLE_AND_ZERO_PADDING];
        for(int i=0; i<SAMPLE_SIZE/DECIM;i++)
            postWindow[i]=inputDataDecim[i]*window[i];
    }

    private double FFTprocesing(){
        DoubleFFT_1D fftdone = new DoubleFFT_1D(SAMPLE_AND_ZERO_PADDING);
        double[] a = new double[FFT_SIZE];
        System.arraycopy(postWindow,0,a,0,SAMPLE_AND_ZERO_PADDING);
        fftdone.realForward(a);
        double [] magid= new double[SAMPLE_AND_ZERO_PADDING];
        for(int i = 0; i < a.length / 2; ++i) {
            double re  = a[2*i];
            double im  = a[2*i+1];
            double mag = Math.sqrt(re * re + im * im);
            magid[i]=mag;
        }
        return pochodna(magid,a.length);
    }
    private double pochodna(double [] magid, int a){
        int k=0;
        double avrMag = 0;
        for(int j=0;j<(magid.length/4)-6;j++){
            avrMag=avrMag+magid[j];}

        avrMag=avrMag/(magid.length/4);
        for(int j=0;j<(magid.length/4)-6;j++){
            if((magid[j+6]-magid[j])<0&&magid[j]>avrMag){
                pocht[k]=new bigEnd1((Fs_RATE/DECIM) * j / (a/2 ),magid[j]);
                k++;
            }
        }
        List<bigEnd1> lista = new ArrayList<bigEnd1>();
        int j=0,y=0,n=0;
        double freq=0;
        boolean doing=true;
        double [] peak= new double [100];
        while(n<k-1){
            for(;n<k-1;n++){
                if (pocht[n].freq + 1 == pocht[n+1].freq) {
                    lista.add(pocht[n]);
                }
                else{
                    n++;
                    break;
                }
            }
            if(lista.size()>3){
                peak[y]=lista.get(3).getFreq();
                y++;
                lista.clear();
                //break;
            }
            else{
                lista.clear();
            }
        }
        while(doing&&j<y){
            for(int i=j+1;i<y;i++){
                if(peak[j]*2.005>peak[i]&&peak[j]*2<peak[i]*1.01){
                    doing=false;
                    freq=peak[j];
                }
            }
            j++;
        }
        return freq;
    }
}}
